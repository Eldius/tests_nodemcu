
function readTemp()
    local dhtPin = 1

    status, temp, humi, temp_dec, humi_dec = dht.read(dhtPin)
    if status == dht.OK then
        print("DHT Temperature:"..temp..";".."Humidity:"..humi)
    elseif status == dht.ERROR_CHECKSUM then
        print( "DHT Checksum error." )
    elseif status == dht.ERROR_TIMEOUT then
        print( "DHT timed out." )
    end
end

function connected()
    print("Connected to wifi as:" .. wifi.sta.getip())
end
function errorConnecting(err, str)
    print("enduser_setup: Err #" .. err .. ": " .. str)
end
function networkSetup()
    enduser_setup.start(
        connected
        , errorConnecting
    );
end

function locals()
    for n in pairs(_G) do print(n) end
end

--print (enduser_setup.manual())

--networkSetup()

print(locals())

readTemp()
readTemp()
readTemp()
readTemp()

print()




